package net.golovach.eshop.dao;

import net.golovach.eshop.dao.exception.DaoSystemException;
import net.golovach.eshop.dao.exception.NoSuchEntityException;
import net.golovach.eshop.entity.Product;

import java.math.BigDecimal;
import java.util.List;

// CRUD operations
// create = SQL/insert = List/add
// read = SQL/select = List/get
// update = SQL/update = List/set
// delete = SQL/delete = List/remove
public interface ProductDao {

    /**
     * Never return null!
     */
    public Product selectById(int id) throws DaoSystemException, NoSuchEntityException;

    public List<Product> selectAll() throws DaoSystemException;
}
