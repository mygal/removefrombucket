package net.golovach.eshop.custom_view_framework;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//todo: realize old session clearing functionality
//todo: realize session listeners functionality
//todo: realize storage session in cookies
//todo: rewrite product bucket to this session implementation (Base64 or "session"->"{paper=1,bread=10}")
public class CustomHttpSessionOnClientRepository {

    public static CustomHttpSession getSession(HttpServletRequest request) {
        throw new UnsupportedOperationException();
    }

    public static void saveSession(HttpServletResponse response, CustomHttpSession session) {
        throw new UnsupportedOperationException();
    }    
}
