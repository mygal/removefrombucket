package net.golovach.eshop.listener.demo;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DemoServletContextListener implements ServletContextListener {
    public DemoServletContextListener() {
        System.out.println(">> MyServletContextListener - NEW");
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        System.out.println(">> ServletContext - created, contextPath = " + event.getServletContext().getContextPath());
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println(">> ServletContext - destroyed, contextPath = " + event.getServletContext().getContextPath());
    }
}
