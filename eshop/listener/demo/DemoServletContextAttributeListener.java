package net.golovach.eshop.listener.demo;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

public class DemoServletContextAttributeListener implements ServletContextAttributeListener {
    public DemoServletContextAttributeListener() {
        System.out.println(">> MyServletContextAttributeListener - NEW");
    }

    @Override
    public void attributeAdded(ServletContextAttributeEvent event) {
        System.out.println(">> ServletContext - attribute added (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent event) {
        System.out.println(">> ServletContext - attribute removed (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent event) {
        System.out.println(">> ServletContext - attribute replaced (" + event.getName() + " => " + event.getValue() + ")");
    }
}
