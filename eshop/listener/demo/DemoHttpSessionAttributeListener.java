package net.golovach.eshop.listener.demo;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class DemoHttpSessionAttributeListener implements HttpSessionAttributeListener {
    public DemoHttpSessionAttributeListener() {
        System.out.println(">> MyHttpSessionAttributeListener - NEW");
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        System.out.println(">> HttpSession - attribute added (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        System.out.println(">> HttpSession - attribute removed (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        System.out.println(">> HttpSession - attribute replaced (" + event.getName() + " => " + event.getValue() + ")");
    }
}
