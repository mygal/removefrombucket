package net.golovach.eshop.listener.demo;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;

public class DemoServletRequestAttributeListener implements ServletRequestAttributeListener {
    public DemoServletRequestAttributeListener() {
        System.out.println(">> MyServletRequestAttributeListener - NEW");
    }

    @Override
    public void attributeAdded(ServletRequestAttributeEvent event) {
        System.out.println(">> ServletRequest - attribute added (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void attributeRemoved(ServletRequestAttributeEvent event) {
        System.out.println(">> ServletRequest - attribute removed (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void attributeReplaced(ServletRequestAttributeEvent event) {
        System.out.println(">> ServletRequest - attribute replaced (" + event.getName() + " => " + event.getValue() + ")");
    }
}
