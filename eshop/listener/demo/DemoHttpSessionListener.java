package net.golovach.eshop.listener.demo;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class DemoHttpSessionListener implements HttpSessionListener {
    public DemoHttpSessionListener() {
        System.out.println(">> MyHttpSessionListener - NEW");
    }

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        System.out.println(">> HttpSession - created, id = " + event.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        System.out.println(">> HttpSession - destroyed, id = " + event.getSession().getId());
    }
}
