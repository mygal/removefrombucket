package net.golovach.eshop.listener.demo;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class DemoHttpSessionBindingListener implements HttpSessionBindingListener {
    public DemoHttpSessionBindingListener() {
        System.out.println(">> MyHttpSessionBindingListener - NEW");
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        System.out.println(">> HttpSession - value bound (" + event.getName() + " => " + event.getValue() + ")");
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        System.out.println(">> HttpSession - value unbound (" + event.getName() + " => " + event.getValue() + ")");
    }
}
