package net.golovach.eshop.listener.demo;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class DemoServletRequestListener implements ServletRequestListener {
    public DemoServletRequestListener() {
        System.out.println(">> MyServletRequestListener - NEW");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println(">> ServletRequest - initialized");
    }

    @Override
    public void requestDestroyed(ServletRequestEvent event) {
        System.out.println(">> ServletRequest - destroyed");
    }
}
