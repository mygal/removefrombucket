package net.golovach.eshop.listener.demo;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;

public class DemoHttpSessionActivationListener implements HttpSessionActivationListener {
    public DemoHttpSessionActivationListener() {
        System.out.println(">> MyHttpSessionActivationListener - NEW");
    }

    @Override
    public void sessionWillPassivate(HttpSessionEvent event) {
        System.out.println(">> HttpSession - will passivate, id = " + event.getSession().getId());
    }

    @Override
    public void sessionDidActivate(HttpSessionEvent event) {
        System.out.println(">> HttpSession - did activate, id = " + event.getSession().getId());
    }
}
